import type { FC, PropsWithChildren } from 'react';

import HomeLayout from '@/layouts/Home';

const Layout: FC<PropsWithChildren> = ({ children }) => {
  return <HomeLayout>{children}</HomeLayout>;
};

export default Layout;
