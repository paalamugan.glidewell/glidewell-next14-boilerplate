import { Heading } from '@glidewell/react-layouts';
import type { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'About',
  description: 'About page',
};

const About = () => {
  return <Heading as="h1">Welcome to our About page</Heading>;
};

export default About;
