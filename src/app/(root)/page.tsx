import { Button } from '@glidewell/react-components';
import { Heading, Stack } from '@glidewell/react-layouts';
import type { Metadata } from 'next';

import Link from '@/components/common/Link';

export const metadata: Metadata = {
  title: 'Home',
  description: 'Home page',
};

const Home = () => {
  return (
    <Stack justifyContent="center" alignItems="center">
      <Heading as="h1">Welcome to our Home page</Heading>
      <Link href="/dashboard">
        <Button variant="outline">Go to Dashboard</Button>
      </Link>
    </Stack>
  );
};

export default Home;
