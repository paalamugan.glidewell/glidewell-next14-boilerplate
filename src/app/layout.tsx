import '@/styles/globals.css';

import { NextThemeProvider } from '@glidewell/react-providers/NextThemeProvider';
import { cn } from '@glidewell/react-shared/lib';
import type { Metadata } from 'next';
import NextTopLoader from 'nextjs-toploader';
import type { FC, PropsWithChildren } from 'react';

import { appConfig } from '@/constants/app-config.constant';
import BaseLayout from '@/layouts/Base';
import { INTER, OPEN_SANS } from '@/lib/next-fonts';
import { TRPCReactProvider } from '@/trpc/client';

const fontClasses = cn(OPEN_SANS.variable, INTER.variable);

export const metadata: Metadata = {
  title: appConfig.title,
  description: appConfig.description,
  icons: [
    {
      rel: 'icon',
      url: '/favicon.png',
    },
  ],
};

const RootLayout: FC<PropsWithChildren> = async ({ children }) => {
  return (
    <html lang="en" className={fontClasses} suppressHydrationWarning>
      <body suppressHydrationWarning>
        <NextThemeProvider defaultTheme="light">
          <TRPCReactProvider>
            <NextTopLoader height={5} />
            <BaseLayout>{children}</BaseLayout>
          </TRPCReactProvider>
        </NextThemeProvider>
      </body>
    </html>
  );
};

export default RootLayout;
