import { Button } from '@glidewell/react-components';
import { Box, Strong, Text } from '@glidewell/react-layouts';

import Link from '@/components/common/Link';
import { getAuthUser } from '@/server/auth';

import { HelloClient } from './HelloClient';

const Hello = async () => {
  const user = await getAuthUser();

  return (
    <>
      <Text fontSize="2xl">
        👋 Hello <Strong>{user?.userName || 'N/A'}</Strong>
      </Text>
      <Box as="pre">
        <HelloClient />
      </Box>
      <Link href="/dashboard/user-profile" className="cursor-pointer">
        <Button variant="link" color="blue">
          Go to Profile Page
        </Button>
      </Link>
    </>
  );
};

export { Hello };
