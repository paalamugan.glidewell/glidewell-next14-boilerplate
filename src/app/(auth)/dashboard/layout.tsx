import type { FC, PropsWithChildren } from 'react';

import DefaultLayout from '@/layouts/Default';

const DashboardLayout: FC<PropsWithChildren> = ({ children }) => {
  return <DefaultLayout>{children}</DefaultLayout>;
};

export default DashboardLayout;
