import { Hello } from '@/app/(auth)/dashboard/_components/Hello';

export async function generateMetadata() {
  return {
    title: 'Dashboard',
  };
}

const Dashboard = async () => {
  return (
    <div className="[&_p]:my-6">
      <Hello />
    </div>
  );
};

export default Dashboard;
