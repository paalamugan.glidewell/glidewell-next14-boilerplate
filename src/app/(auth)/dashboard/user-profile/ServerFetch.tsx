import { Box, Heading } from '@glidewell/react-layouts';

import { api } from '@/trpc/server';

export const ServerFetch = async () => {
  const data = await api.auth.authToken();
  return (
    <Box className="overflow-hidden [&_p]:my-6">
      <Heading as="h5">Data fetched in Server</Heading>
      <Box as="pre" className="rounded-lg bg-muted p-4">
        {JSON.stringify(data, null, 2)}
      </Box>
    </Box>
  );
};
