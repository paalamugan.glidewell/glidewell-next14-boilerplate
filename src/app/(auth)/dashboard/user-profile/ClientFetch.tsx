'use client';

import { Box, Heading } from '@glidewell/react-layouts';

import { api } from '@/trpc/client';

export const ClientFetch = () => {
  const { data, isPending } = api.auth.authToken.useQuery();
  return (
    <Box className="overflow-hidden [&_p]:my-6">
      <Heading as="h5">Data fetched in Client</Heading>
      <Box as="pre" className="rounded-lg bg-muted p-4">
        {isPending ? 'Loading...' : JSON.stringify(data, null, 2)}
      </Box>
    </Box>
  );
};
