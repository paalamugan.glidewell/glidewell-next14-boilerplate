import { Box, Flex, Grid, GridItem, Heading } from '@glidewell/react-layouts';

import Link from '@/components/common/Link';

import { ClientFetch } from './ClientFetch';
import { ServerFetch } from './ServerFetch';

export async function generateMetadata() {
  return {
    title: 'User Profile',
  };
}

const UserProfilePage = () => {
  return (
    <Box className="my-6">
      <Flex className="mb-4" mb="4" justifyContent="between">
        <Heading as="h1">User Data</Heading>
        <Link href="/dashboard" className="ml-5 text-blue-500 underline">
          Go to Dashboard
        </Link>
      </Flex>
      <Grid gridCols="2" gap="7">
        <GridItem>
          <ClientFetch />
        </GridItem>
        <GridItem>
          <ServerFetch />
        </GridItem>
      </Grid>
    </Box>
  );
};

export default UserProfilePage;
