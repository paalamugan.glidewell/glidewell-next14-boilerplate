import type { FC, PropsWithChildren } from 'react';

import { AuthGuard } from '@/components/AuthGuard';
import { SessionProvider } from '@/providers/SessionProvider';
import { getAuthSession } from '@/server/auth';

const AuthLayout: FC<PropsWithChildren> = async ({ children }) => {
  const session = await getAuthSession();

  return (
    <SessionProvider session={session}>
      <AuthGuard>{children}</AuthGuard>
    </SessionProvider>
  );
};

export default AuthLayout;

export const dynamic = 'force-dynamic';
