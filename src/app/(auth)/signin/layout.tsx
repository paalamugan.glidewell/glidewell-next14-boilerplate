import type { FC, PropsWithChildren } from 'react';

import NavBar from '@/components/Containers/NavBar';
import CenteredLayout from '@/layouts/Centered';

const Layout: FC<PropsWithChildren> = ({ children }) => {
  return (
    <>
      <NavBar navItems={[]} />
      <CenteredLayout>{children}</CenteredLayout>
    </>
  );
};

export default Layout;
