import { LockClosedIcon } from '@glidewell/react-icons/solid';
import { Box, H2, Paper, Text } from '@glidewell/react-layouts';
import Image from 'next/image';

import Link from '@/components/common/Link';

import { SignInForm } from './_components/SignInForm';

export async function generateMetadata() {
  return {
    title: 'Sign In',
    description: 'Sign in to your account',
  };
}

const SignInPage = async (props: { searchParams: { callbackUrl: string | undefined } }) => {
  const { searchParams } = props;
  return (
    <Paper withBorder>
      <Box className="my-6 flex min-h-full min-w-[30rem] flex-col justify-center px-6 py-12 lg:px-8">
        <Box className="sm:mx-auto sm:w-full sm:max-w-sm">
          <Image
            className="mx-auto h-10 w-auto"
            src="/static/images/glidewell-logo.png"
            alt="Glidewell Logo"
            width={200}
            height={50}
          />
          <H2 className="mt-10 flex items-center justify-center gap-1 text-center text-2xl font-bold leading-9 tracking-tight">
            <LockClosedIcon className="size-7" /> Sign in to your account
          </H2>
        </Box>

        <Box className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
          <SignInForm callbackUrl={searchParams.callbackUrl} />
        </Box>

        <Text className="mt-6 text-center">
          <Link href="/" className="text-blue-700 hover:underline">
            Go to Home
          </Link>
        </Text>
      </Box>
    </Paper>
  );
};

export default SignInPage;
