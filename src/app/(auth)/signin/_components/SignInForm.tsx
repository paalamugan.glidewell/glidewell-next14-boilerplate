'use client';

import { Form, type FormFieldItem, toast } from '@glidewell/react-components';
import { zodResolver } from '@hookform/resolvers/zod';
import { signIn } from 'next-auth/react';
import { useForm } from 'react-hook-form';

import { type LoginValidationSchema, loginValidationSchema } from '@/validations/auth.validation';

const fields: FormFieldItem<LoginValidationSchema>[] = [
  {
    type: 'input',
    autoComplete: 'off',
    label: 'Username',
    name: 'username',
    placeholder: 'Enter a username',
    required: true,
  },
  {
    type: 'input',
    autoComplete: 'off',
    label: 'Password',
    name: 'password',
    placeholder: 'Enter a password',
    required: true,
    inputType: 'password',
  },
];

export const SignInForm = (props: { callbackUrl: string | undefined }) => {
  const { callbackUrl = '/dashboard' } = props;

  const form = useForm<LoginValidationSchema>({
    resolver: zodResolver(loginValidationSchema),
    defaultValues: {
      username: '',
      password: '',
    },
  });

  const onSubmit = async (data: LoginValidationSchema) => {
    const { username, password } = data;
    try {
      await signIn('credentials', {
        username,
        password,
        callbackUrl,
      });
    } catch (error) {
      toast.error('Invalid username or password');
    }
  };

  return (
    <div className="space-y-6">
      <Form<LoginValidationSchema>
        form={form}
        fields={fields}
        onSubmit={onSubmit}
        hideResetButton
        submitClassName="w-full"
        submitText="Sign in"
      />
    </div>
  );
};
