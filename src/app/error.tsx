/* eslint-disable no-console */

'use client';

import { Button } from '@glidewell/react-components';
import { ArrowRightIcon } from '@glidewell/react-icons/outline';
import { Box, Flex, Strong, Text } from '@glidewell/react-layouts';
import { cn } from '@glidewell/react-shared/lib';
import { useRouter } from 'next/navigation';
import { type FC, useEffect } from 'react';

import Link from '@/components/common/Link';
import CenteredLayout from '@/layouts/Centered';

type ErrorPageProps = {
  error: Error;
  statusCode?: number;
  statusText?: string;
  showErrorMessage?: boolean;
};
const ErrorPage: FC<ErrorPageProps> = ({
  error,
  statusCode = 500,
  statusText = 'Internal Server Error',
  showErrorMessage = false,
}) => {
  const router = useRouter();
  useEffect(() => {
    console.error(error);
  }, [error]);

  const onRefresh = () => {
    router.refresh();
  };
  const textColor = statusCode >= 400 && showErrorMessage ? 'text-danger' : '';
  return (
    <>
      <div />
      <CenteredLayout>
        <main className="flex flex-col gap-3 text-center">
          <h1 className={cn('text-4xl font-semibold', textColor)}> {statusCode} </h1>
          <h1 className={cn('mt-3', textColor)}>{statusText}</h1>
          <Box className="mt-3 max-w-sm text-center text-lg">
            {showErrorMessage ? (
              <Text color="danger" mb="3">
                <Strong>Error Message:</Strong> {error.message}
              </Text>
            ) : (
              <Text>
                This page is currently unavailable. Please see the console for more information.
              </Text>
            )}
          </Box>
          <Flex justifyContent="center" gap="3">
            <Button as={Link} href="/dashboard">
              Back to Dashboard
              <ArrowRightIcon className="size-5" />
            </Button>
            <Button variant="outline" onClick={onRefresh}>
              Refresh
            </Button>
          </Flex>
        </main>
      </CenteredLayout>
    </>
  );
};

export default ErrorPage;
