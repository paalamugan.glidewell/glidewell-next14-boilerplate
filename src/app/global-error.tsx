/* eslint-disable no-console */

'use client';

import { type FC, useEffect } from 'react';

import BaseLayout from '@/layouts/Base';

import ErrorPage from './error';

type GlobalErrorPageProps = {
  error: Error & { digest?: string };
};
const GlobalErrorPage: FC<GlobalErrorPageProps> = ({ error }) => {
  useEffect(() => {
    console.error('error', error);
  }, [error]);

  return (
    <html lang="en">
      <body>
        <BaseLayout>
          <ErrorPage error={error} />
        </BaseLayout>
      </body>
    </html>
  );
};

export default GlobalErrorPage;
