'use client';

import { Button } from '@glidewell/react-components';
import { ArrowRightIcon } from '@glidewell/react-icons/solid';

import Link from '@/components/common/Link';
import CenteredLayout from '@/layouts/Centered';

const NotFoundPage = () => {
  return (
    <>
      <div />
      <CenteredLayout>
        <main className="flex flex-col gap-3 text-center">
          <h1 className="text-4xl font-semibold"> 404 </h1>
          <h1 className="mt-4">Page could not be found</h1>
          <p className="mt-4 max-w-sm text-center text-lg">
            Sorry, the page you are looking for does not exist.
          </p>
          <Button as={Link} href="/dashboard">
            Back to Dashboard
            <ArrowRightIcon className="size-5" />
          </Button>
        </main>
      </CenteredLayout>
    </>
  );
};

export default NotFoundPage;
