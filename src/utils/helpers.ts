import type { RichTranslationValues } from '@/types';

export const getClientBaseUrl = () => {
  if (process.env.NEXT_PUBLIC_APP_URL) return process.env.NEXT_PUBLIC_APP_URL;
  if (process.env.NEXTAUTH_URL) return process.env.NEXTAUTH_URL;
  return `http://localhost:${process.env.PORT ?? 3000}`;
};

export const getServerBaseUrl = () => {
  if (process.env.NEXTAUTH_URL !== undefined) return process.env.NEXTAUTH_URL;
  return `http://localhost:${process.env.PORT ?? 3000}`;
};

export const replaceLinkWithContext = (link: string, context?: RichTranslationValues) => {
  return Object.entries(context || {}).reduce(
    (finalLink, [find, replace]) =>
      finalLink.replace(`{${find}}`, typeof replace === 'string' ? replace : ''),
    link
  );
};

/**
 * Checks if a token has expired.
 * @param expireDate - The expiration date of the token.
 * @param beforeExpireInMin - The number of minutes before the token actually expires to consider it as expired. Default is 0.
 * @returns A boolean indicating whether the token has expired.
 */
export const isTokenExpired = (
  expireDate: string | Date,
  beforeExpireInMin: number = 0
): boolean => {
  const now = new Date().getTime();
  const minutesInMilliSecond = beforeExpireInMin * 60 * 1000;

  /**
   * Subtract the specified number of minutes from the expiration date, so that we can consider the token as expired before it actually expires.
   * The reason we are do this is, We need ta valid token to generate a new token before it expires through the refresh token API.
   */
  const expireDateWithBuffer = new Date(new Date(expireDate).getTime() - minutesInMilliSecond);

  return now > expireDateWithBuffer.getTime();
};
