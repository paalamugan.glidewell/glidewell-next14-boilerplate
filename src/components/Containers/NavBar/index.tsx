'use client';

import { Label } from '@glidewell/react-components';
import { Bars3Icon as Hamburger, XMarkIcon as XMark } from '@glidewell/react-icons/solid';
import { useSession } from 'next-auth/react';
import type { FC, HTMLAttributeAnchorTarget } from 'react';
import { useState } from 'react';

import Link from '@/components/common/Link';
import ThemeToggle from '@/components/common/ThemeToggle';
import NavItem from '@/components/Containers/NavBar/NavItem';
import { SignOutButton } from '@/components/SignOutButton';
import { appConfig } from '@/constants/app-config.constant';

import style from './index.module.css';

const navInteractionIcons = {
  show: <Hamburger className={style.navInteractionIcon} />,
  close: <XMark className={style.navInteractionIcon} />,
};

export type NavbarProps = {
  navItems: Array<{
    text: string;
    link: string;
    target?: HTMLAttributeAnchorTarget | undefined;
    icon?: React.ComponentType<{ className: string | undefined }>;
  }>;
};

const NavBar: FC<NavbarProps> = ({ navItems }) => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const session = useSession();

  return (
    <nav className={`${style.container}`}>
      <div className={style.demoIconAndMobileItemsToggler}>
        <Link className={style.demoIconWrapper} href="/" aria-label="Home">
          {appConfig.title}
        </Link>

        <Label
          onClick={() => setIsMenuOpen(prev => !prev)}
          className={style.sidebarItemTogglerLabel}
          htmlFor="sidebarItemToggler"
        >
          {navInteractionIcons[isMenuOpen ? 'close' : 'show']}
        </Label>
      </div>

      <input className="peer hidden" id="sidebarItemToggler" type="checkbox" />

      <div className={`${style.main} peer-checked:flex`}>
        <div className={style.navItems}>
          {navItems.map(({ text, link, target, icon }) => (
            <NavItem key={link} href={link} target={target} icon={icon}>
              {text}
            </NavItem>
          ))}
        </div>

        <div className={style.actionsWrapper}>
          <ThemeToggle />
          {session.status === 'authenticated' && <SignOutButton />}
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
