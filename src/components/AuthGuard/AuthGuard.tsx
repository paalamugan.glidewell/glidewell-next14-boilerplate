'use client';

import { ArrowPathIcon } from '@glidewell/react-icons/solid';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import { useSession } from 'next-auth/react';
import { type PropsWithChildren, useEffect } from 'react';

export const AuthGuard: React.FC<PropsWithChildren> = ({ children }) => {
  const session = useSession();
  const router = useRouter();
  const pathname = usePathname();
  const searchParams = useSearchParams();

  useEffect(() => {
    if (pathname === '/signin' && session.status === 'authenticated') {
      router.push('/dashboard');
    }

    if (session.status === 'unauthenticated') {
      router.push(`/signin?${searchParams.toString()}`);
    }
  }, [session.status, pathname, router, searchParams]);

  if (
    session.status === 'loading' ||
    (session.status === 'unauthenticated' && pathname !== '/signin')
  ) {
    return (
      <div className="flex h-screen items-center justify-center">
        <ArrowPathIcon className="size-6 animate-spin" />
      </div>
    );
  }

  return children;
};
