import { Button } from '@glidewell/react-components';
import { MoonIcon, SunIcon } from '@glidewell/react-icons/outline';
import { useNextTheme } from '@glidewell/react-providers/NextThemeProvider';
import { cn } from '@glidewell/react-shared/lib';
import type { FC, MouseEvent } from 'react';

import styles from './index.module.css';

type ThemeToggleProps = {
  onClick?: (event: MouseEvent<HTMLButtonElement>) => void;
};
const ThemeToggle: FC<ThemeToggleProps> = ({ onClick }) => {
  const ariaLabel = 'Toggle theme';
  const { resolvedTheme, setTheme } = useNextTheme();

  const toggleCurrentTheme = (event: MouseEvent<HTMLButtonElement>) => {
    setTheme(resolvedTheme === 'dark' ? 'light' : 'dark');
    onClick?.(event);
  };
  return (
    <Button
      type="button"
      onClick={toggleCurrentTheme}
      className={cn(styles.themeToggle, 'hover:dark:bg-neutral-800')}
      aria-label={ariaLabel}
    >
      <MoonIcon className="block dark:hidden" height="20" />
      <SunIcon className="hidden dark:block" height="20" />
    </Button>
  );
};

export default ThemeToggle;
