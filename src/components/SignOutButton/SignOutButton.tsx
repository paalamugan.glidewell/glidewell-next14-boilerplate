'use client';

import { Button } from '@glidewell/react-components';
import { UserIcon } from '@glidewell/react-icons/solid';
import { signOut } from 'next-auth/react';

export const SignOutButton = () => {
  return (
    <Button
      type="button"
      leftIcon={<UserIcon className="size-5" />}
      className="text-nowrap text-white"
      onClick={async () => {
        await signOut({
          callbackUrl: '/signin',
        });
      }}
    >
      Sign Out
    </Button>
  );
};
