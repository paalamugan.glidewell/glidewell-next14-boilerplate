import { Box } from '@glidewell/react-layouts';
import type { FC, PropsWithChildren } from 'react';

import CenteredLayout from '@/layouts/Centered';

import styles from './layouts.module.css';

const HomeLayout: FC<PropsWithChildren> = ({ children }) => (
  <>
    <Box />
    <CenteredLayout>
      <Box as="main" className={styles.homeLayout}>
        {children}
      </Box>
    </CenteredLayout>
  </>
);

export default HomeLayout;
