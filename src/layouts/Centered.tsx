import { Box } from '@glidewell/react-layouts';
import type { FC, PropsWithChildren } from 'react';

import styles from './layouts.module.css';

const CenteredLayout: FC<PropsWithChildren> = ({ children }) => (
  <Box className={styles.centeredLayout}>{children}</Box>
);

export default CenteredLayout;
