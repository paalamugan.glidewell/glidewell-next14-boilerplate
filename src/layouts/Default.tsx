import { Box } from '@glidewell/react-layouts';
import type { FC, PropsWithChildren } from 'react';

import WithNavBar from '@/components/withNavBar';

const DefaultLayout: FC<PropsWithChildren> = ({ children }) => (
  <>
    <WithNavBar />
    <Box as="main">{children}</Box>
  </>
);

export default DefaultLayout;
