'use client';

import { Box } from '@glidewell/react-layouts';
import type { FC, PropsWithChildren } from 'react';

import styles from './layouts.module.css';

type BaseLayoutProps = PropsWithChildren;
const BaseLayout: FC<BaseLayoutProps> = ({ children }) => {
  return <Box className={styles.baseLayout}>{children}</Box>;
};

export default BaseLayout;
