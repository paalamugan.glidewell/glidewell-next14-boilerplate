import { z } from 'zod';

export const loginValidationSchema = z.object({
  username: z.string().min(1),
  password: z.string().min(1),
});

export type LoginValidationSchema = z.infer<typeof loginValidationSchema>;
