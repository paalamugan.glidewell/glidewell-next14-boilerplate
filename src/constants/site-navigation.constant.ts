import type { SiteNavigation } from '@/types';

// FIXME: This is a placeholder for the site navigation. Update this object to match the navigation items for your site.
export const siteNavigation: SiteNavigation = {
  topNavigation: {
    dashboard: {
      label: 'Dashboard',
      link: '/dashboard',
    },
  },
  sideNavigation: {
    dashboard: {
      label: 'dashboard',
      link: '/dashboard',
    },
  },
};
