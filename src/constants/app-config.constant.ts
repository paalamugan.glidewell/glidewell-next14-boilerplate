// FIXME: Update this configuration file based on your project information
export const appConfig = {
  title: 'Next14 Boilerplate With Next Auth',
  description:
    'Boilerplate and Starter for Next.js 14+ with App Router support, Next Auth, TRPC, Tailwind CSS and TypeScript.',
};
