export const AUTH_BASE_PATHS = {
  TOKEN: '/api/token',
  REFRESH_TOKEN: '/api/refreshtoken',
} as const;
