import { z } from 'zod';

export const loginInputSchema = z.object({
  credentials: z.object({
    username: z.string(),
    password: z.string(),
  }),
});
export type LoginInputSchema = z.infer<typeof loginInputSchema>;

export const refreshTokenInputSchema = z.object({
  token: z.string(),
});

export type RefreshTokenInputSchema = z.infer<typeof refreshTokenInputSchema>;
