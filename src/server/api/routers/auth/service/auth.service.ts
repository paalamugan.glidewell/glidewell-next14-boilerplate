import { getTRPCError } from '@/server/api/common/errors/trpc-error';
import { Logger } from '@/server/api/common/logger';
import { appSettingService } from '@/server/api/common/services/app-setting';
import { FetchRequestService } from '@/server/api/common/services/fetch-request';
import { generateExpireDate, sanitizePathUrl } from '@/server/api/common/utils/utils';
import {
  type AuthToken,
  type LoginArgs,
  type LogoutArgs,
} from '@/server/api/routers/auth/service/auth.service.types';

import { AUTH_BASE_PATHS } from '../paths';
import { tokenResponseSchema } from './auth.service.schema';

class AuthService {
  private readonly logger = new Logger(AuthService.name);

  get requestInstance() {
    const authBaseEndpointUrl = appSettingService.getServiceEndpointUrl('authBase');
    return new FetchRequestService(authBaseEndpointUrl);
  }

  async getToken(credentials: LoginArgs['input']['credentials']) {
    const sanitizedPath = sanitizePathUrl(AUTH_BASE_PATHS.TOKEN, {});
    return this.requestInstance.post(sanitizedPath, {
      responseValidation: tokenResponseSchema,
      data: {
        userName: credentials.username,
        password: credentials.password,
      },
    });
  }

  async refreshToken(token: string) {
    const request = this.requestInstance;
    request.setAuthToken(token);

    const sanitizedPath = sanitizePathUrl(AUTH_BASE_PATHS.REFRESH_TOKEN, {});

    const result = await request.post(sanitizedPath, {
      responseValidation: tokenResponseSchema,
    });

    if (!result.success) {
      throw getTRPCError(result.error, 'UNAUTHORIZED');
    }

    return { ...result.data, expireDate: generateExpireDate(result.data.expiresIn) };
  }

  public async signIn({ input }: LoginArgs): Promise<AuthToken> {
    try {
      const { credentials } = input;
      // Initialize the app settings
      await appSettingService.init(true);
      // Fetch the token from the auth service
      const result = await this.getToken(credentials);

      if (!result.success) {
        throw new Error(result.error.message, { cause: result.error });
      }
      return {
        ...result.data,
        expireDate: generateExpireDate(result.data.expiresIn),
      };
    } catch (error: unknown) {
      throw getTRPCError(error, 'UNAUTHORIZED');
    }
  }

  public async signOut(args: LogoutArgs): Promise<void> {
    this.logger.info('User signed out', args);
  }
}

export const authService = new AuthService();
