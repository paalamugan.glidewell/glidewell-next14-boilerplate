import { z } from 'zod';

export const tokenResponseSchema = z.object({
  claims: z.array(z.string()),
  expiresIn: z.number(),
  token: z.string(),
  userName: z.string(),
});

export type TokenResponseSchema = z.infer<typeof tokenResponseSchema>;
