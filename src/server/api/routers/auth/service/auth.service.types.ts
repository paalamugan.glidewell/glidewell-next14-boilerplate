import type { TRPCContext } from '@/server/api/trpc';

import type { LoginInputSchema } from '../auth.input';
import type { TokenResponseSchema } from './auth.service.schema';

export type AuthToken = TokenResponseSchema & { expireDate: string };

export type LoginArgs = {
  input: LoginInputSchema;
  ctx: TRPCContext;
};

export type LogoutArgs = {
  ctx: TRPCContext;
  authToken: AuthToken;
};
