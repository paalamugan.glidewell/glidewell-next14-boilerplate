import { Logger } from '@/server/api/common/logger';
import { type AuthTokenQueryResult } from '@/server/api/routers/auth/auth.types';
import { authService } from '@/server/api/routers/auth/service/auth.service';
import { createTRPCRouter, protectedProcedure, publicProcedure } from '@/server/api/trpc';

import { getTRPCError } from '../../common/errors/trpc-error';
import { loginInputSchema, refreshTokenInputSchema } from './auth.input';
import type { AuthToken } from './service/auth.service.types';

export const authRouter = createTRPCRouter({
  signIn: publicProcedure
    .input(loginInputSchema)
    .mutation(async ({ input, ctx }): Promise<AuthToken> => {
      return authService.signIn({ input, ctx });
    }),

  authToken: protectedProcedure.query(async ({ ctx }): Promise<AuthTokenQueryResult> => {
    return ctx.authToken;
  }),

  refreshToken: publicProcedure
    .input(refreshTokenInputSchema)
    .mutation(async ({ input }): Promise<AuthToken> => {
      return authService.refreshToken(input.token);
    }),

  signOut: protectedProcedure.mutation(async ({ ctx }) => {
    try {
      await authService.signOut({
        authToken: ctx.authToken,
        ctx,
      });
    } catch (error: unknown) {
      Logger.error('Failed to logout', error);
      throw getTRPCError(error);
    }
  }),
});
