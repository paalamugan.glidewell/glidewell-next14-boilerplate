import type { GetParamsFromPath } from '@/types/common';

/**
 * Generates an expiration date in ISO string format.
 *
 * @param expiresInMin - The number of minutes until the expiration date.
 * @returns The expiration date as an ISO string.
 */
export const generateExpireDate = (expiresInMin: number) => {
  const expiresMinToSec = expiresInMin * 60;
  const expiresSecToMilliSec = expiresMinToSec * 1000;
  return new Date(new Date().getTime() + expiresSecToMilliSec).toISOString();
};

/**
 * Sanitizes a URL path by replacing parameter placeholders with actual values.
 * @param path - The URL path containing parameter placeholders.
 * @param params - An object containing key-value pairs where the key is the placeholder name and the value is the replacement string.
 * @returns - The sanitized URL path with placeholders replaced by actual values.
 */
export const sanitizePathUrl = <T extends string>(
  path: T,
  ...[params]: GetParamsFromPath<T> extends undefined ? [] : [GetParamsFromPath<T>]
) => {
  if (!params) return path;
  return Object.entries(params).reduce(
    (finalPath, [find, replace]) =>
      finalPath.replace(`:${find}`, typeof replace === 'string' ? replace : '') as T,
    path
  );
};
