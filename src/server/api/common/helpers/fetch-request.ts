import type { z } from 'zod';
import { fromError, type FromZodErrorOptions } from 'zod-validation-error';

import { ApiValidationError, isApiValidationError } from '../errors/api-validation.error';
import { logger } from '../logger';

export type FetchRequestOptions<T extends z.ZodTypeAny> = RequestInit & {
  /**
   * The Zod schema to validate the response data. The response data will be parsed and validated using this schema.
   */
  responseValidation: T;
  /**
   * The headers to include in the fetch request.
   */
  headers?: Record<string, string>;
  /**
   * Callback to handle the raw response before parsing it.
   */
  onRawResponse?: (response: Response) => void;
  /**
   *
   * @param data  - The raw response data before parsing it.
   * @returns  - nothing
   */
  onSuccessRawResponse?: (data: unknown) => void;
  /**
   *
   * @param error - The raw response data before parsing it.
   * @returns  - nothing
   */
  onErrorRawResponse?: (error: unknown) => void;
  /**
   * Callback to handle the parsed response data after validation.
   */
  onSuccess?: (data: z.output<T>) => void;
  /**
   *
   * @param error - The error that occurred during the fetch request.
   * @returns
   */
  onError?: (error: ApiValidationError) => void;
  /**
   * Options for parsing errors using Zod.
   * More info:  check out the Zod-Validation-Error package. (https://www.npmjs.com/package/zod-validation-error)
   */
  zodErrorOptions?: FromZodErrorOptions;
  /**
   * Whether the response is expected to be JSON. If false, the response will be parsed as text. Default is true.
   */
  isJsonResponse?: boolean;
};
export type FetchRequestResponse<T> =
  | { success: true; data: T }
  | { success: false; error: ApiValidationError };

/**
 * Makes a fetch request to the specified URL with the given options.
 * @param url - The URL to make the fetch request to.
 * @param options - The options for the fetch request, including the Zod schema for parsing the response data.
 * @returns A promise that resolves to the parsed response data, or an object with success set to false and the error details if an error occurs.
 */
export const fetchRequest = async <TSchema extends z.ZodTypeAny>(
  url: string | URL | Request,
  options: FetchRequestOptions<TSchema>
): Promise<FetchRequestResponse<z.output<TSchema>>> => {
  const {
    responseValidation,
    onRawResponse,
    onSuccessRawResponse,
    onErrorRawResponse,
    onSuccess,
    onError,
    zodErrorOptions,
    isJsonResponse = true,
    ...fetchOptions
  } = options;
  try {
    const response = await fetch(url, fetchOptions);
    onRawResponse?.(response);
    if (!response.ok) {
      const errorMessage = (await response.text()) || response.statusText;
      throw new ApiValidationError(errorMessage, response.status);
    }

    const result = isJsonResponse ? await response.json() : await response.text();
    onSuccessRawResponse?.(result);
    const data = responseValidation.parse(result);
    onSuccess?.(data);
    return { success: true, data };
  } catch (err) {
    onErrorRawResponse?.(err);
    const zodError = fromError(err, zodErrorOptions);
    const apiError = isApiValidationError(err)
      ? err
      : new ApiValidationError(zodError.message, 1000, zodError);
    onError?.(apiError);
    logger.error(
      'Error making fetch request URL:',
      url,
      '\nFetchOptions: ',
      fetchOptions,
      '\nError:',
      zodError
    );
    return {
      success: false,
      error: apiError,
    };
  }
};
