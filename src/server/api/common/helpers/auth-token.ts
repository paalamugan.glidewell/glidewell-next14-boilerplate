import { api } from '@/trpc/server';

/**
 * Retrieves the authentication token.
 *
 * @returns A promise that resolves to the authentication token if successful, or null if an error occurs.
 */
export const getAuthToken = async () => {
  try {
    return await api.auth.authToken();
  } catch {
    return null;
  }
};
