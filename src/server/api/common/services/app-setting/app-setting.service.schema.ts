import { z } from 'zod';

// FIXME: Update this configuration file based on your project information
export const appSettingResponseSchema = z.object({
  application: z.object({
    iD: z.string(),
    description: z.string(),
    sections: z.array(
      z.object({
        name: z.string(),
        description: z.string().nullable(),
        settings: z.array(
          z.object({
            // FIXME: Make sure to update the key values based on your project, We used this key to identify the setting url
            key: z.enum([
              'attributesBase',
              'productsBase',
              'brandBase',
              'productFieldBase',
              'pricingBase',
              'productIndexBase',
              'authBase',
              'productStepsBase',
              'externalAttributeBase',
            ]),
            value: z.string(),
            description: z.string().nullable(),
            overrides: z.string().nullable(),
          })
        ),
      })
    ),
    lastUpdatedDateUTC: z.string(),
  }),
  message: z.null(),
});

export type AppSettingResponseSchemaType = z.infer<typeof appSettingResponseSchema>;
