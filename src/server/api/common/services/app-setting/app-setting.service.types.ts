import type { AppSettingResponseSchemaType } from './app-setting.service.schema';

export type AppSettingServiceName =
  AppSettingResponseSchemaType['application']['sections'][number]['settings'][number]['key'];
