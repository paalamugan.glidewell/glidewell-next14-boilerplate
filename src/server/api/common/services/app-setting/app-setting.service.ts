import { env } from '@/env';
import { fetchRequest } from '@/server/api/common/helpers/fetch-request';
import { Logger } from '@/server/api/common/logger';
import { replaceLinkWithContext } from '@/utils/helpers';

import {
  appSettingResponseSchema,
  type AppSettingResponseSchemaType,
} from './app-setting.service.schema';
import type { AppSettingServiceName } from './app-setting.service.types';

class AppSettingService {
  private readonly logger = new Logger(AppSettingService.name);

  configurationEndpointUrl: string;

  #appSetting: AppSettingResponseSchemaType | null = null;

  get appSetting(): AppSettingResponseSchemaType | null {
    return this.#appSetting;
  }

  constructor() {
    this.configurationEndpointUrl = replaceLinkWithContext(env.PROJECT_CONFIGURATION_URL, {
      '0': env.PROJECT_DEPLOYED_APPLICATION,
    });
    this.#appSetting = null;
  }

  async init(forceFetch = false) {
    if (this.#appSetting && !forceFetch) return this.#appSetting;
    const result = await fetchRequest(this.configurationEndpointUrl, {
      responseValidation: appSettingResponseSchema,
    });
    if (!result.success) {
      this.logger.error('Failed to get app setting configuration', result.error.cause);
      this.#appSetting = null;
    } else {
      this.#appSetting = result.data;
    }
    return this.#appSetting;
  }

  getServiceEndpointUrl(serviceName: AppSettingServiceName) {
    if (!this.#appSetting) {
      throw new Error('AppSetting not initialized');
    }

    const section = this.#appSetting.application.sections.find(s => s.name === 'BaseURLs');
    if (!section) {
      throw new Error('BaseURLs section not found');
    }
    const setting = section.settings.find(s => s.key === serviceName);
    if (!setting || !setting.value) {
      throw new Error(`BaseURLs setting for ${serviceName} not found`);
    }
    return setting.value;
  }

  reset() {
    this.#appSetting = null;
  }
}

export const appSettingService = new AppSettingService();
