import type { z } from 'zod';

import { getServerBaseUrl } from '@/utils/helpers';

import { fetchRequest, type FetchRequestOptions } from '../../helpers/fetch-request';

export class FetchRequestService {
  readonly baseUrl: string;

  readonly headers: Map<string, string> = new Map();

  constructor(baseUrl: string, headers?: Record<string, string>) {
    this.baseUrl = baseUrl;
    this.headers.set('Content-Type', 'application/json');
    this.setHeaders(headers);
  }

  setHeaders(headers: Record<string, string> | undefined) {
    if (!headers) return;
    Object.keys(headers).forEach(key => {
      const value = headers[key];
      if (value) {
        this.headers.set(key, value);
      }
    });
  }

  getHeaders() {
    return Object.fromEntries(this.headers.entries());
  }

  setAuthToken(token: string) {
    this.headers.set('Authorization', `Token ${token}`);
  }

  fetch<TSchema extends z.ZodTypeAny>(path: string, options: FetchRequestOptions<TSchema>) {
    const url = new URL(path, this.baseUrl);

    const { headers, ...restOptions } = options;
    if (headers) {
      this.setHeaders(headers);
    }
    return fetchRequest(url, {
      headers: Object.fromEntries(this.headers.entries()),
      ...restOptions,
    });
  }

  get<TSchema extends z.ZodTypeAny>(path: string, options: FetchRequestOptions<TSchema>) {
    return this.fetch(path, { ...options, method: 'GET' });
  }

  post<TSchema extends z.ZodTypeAny>(
    path: string,
    options: FetchRequestOptions<TSchema> & { data?: Record<string, unknown> | Array<unknown> }
  ) {
    return this.fetch(path, {
      ...options,
      method: 'POST',
      body: options.body || JSON.stringify(options.data),
    });
  }

  put<TSchema extends z.ZodTypeAny>(
    path: string,
    options: FetchRequestOptions<TSchema> & { data?: Record<string, unknown> | Array<unknown> }
  ) {
    return this.fetch(path, {
      ...options,
      method: 'PUT',
      body: options.body || JSON.stringify(options.data),
    });
  }

  patch<TSchema extends z.ZodTypeAny>(
    path: string,
    options: FetchRequestOptions<TSchema> & { data?: Record<string, unknown> | Array<unknown> }
  ) {
    return this.fetch(path, {
      ...options,
      method: 'PATCH',
      body: options.body || JSON.stringify(options.data),
    });
  }

  delete<TSchema extends z.ZodTypeAny>(path: string, options: FetchRequestOptions<TSchema>) {
    return this.fetch(path, { ...options, method: 'DELETE' });
  }
}

export const createRequestInstance = (baseUrl: string, headers?: Record<string, string>) => {
  return new FetchRequestService(baseUrl, headers);
};

export const fetchRequestService = new FetchRequestService(getServerBaseUrl());
