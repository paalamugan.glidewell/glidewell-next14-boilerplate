import { type ErrorOptions, ValidationError } from 'zod-validation-error';

export class ApiValidationError extends ValidationError {
  statusCode: number;

  constructor(message: string, statusCode: number, opts?: ErrorOptions) {
    super(message, opts);
    this.statusCode = statusCode;
    Object.setPrototypeOf(this, ApiValidationError.prototype);
  }
}
export type ApiValidationErrorType = InstanceType<typeof ApiValidationError>;

export const isApiValidationError = (err: unknown): err is ApiValidationError => {
  return err instanceof ApiValidationError && err.statusCode !== undefined;
};
