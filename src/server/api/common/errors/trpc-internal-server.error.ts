import { TRPCError } from '@trpc/server';

export class TRPCInternalServerError extends TRPCError {
  constructor(error: Error | string, cause?: unknown) {
    const message = error instanceof Error ? error.message : error;
    const localCause = error instanceof Error ? error : cause;
    const code = 'INTERNAL_SERVER_ERROR';
    super({ message, code, cause: localCause });
    Object.setPrototypeOf(this, TRPCInternalServerError.prototype);
  }
}
export type TRPCInternalServerErrorType = InstanceType<typeof TRPCInternalServerError>;
