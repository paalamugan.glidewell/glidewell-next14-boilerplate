import type { GetServerSidePropsContext, NextApiRequest, NextApiResponse } from 'next';
import { type AuthOptions, getServerSession } from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';

import { api } from '@/trpc/server';
import { isTokenExpired } from '@/utils/helpers';

import { Logger } from './api/common/logger';

/**
 * Options for NextAuth.js used to configure adapters, providers, callbacks,
 * etc.
 *
 * @see https://next-auth.js.org/configuration/options
 * */
export const authConfig: AuthOptions = {
  session: {
    strategy: 'jwt',
    maxAge: 24 * 60 * 60, // 24 hours
  },
  pages: {
    signIn: '/signin',
    error: '/auth-error',
  },
  providers: [
    CredentialsProvider({
      // The name to display on the sign in form (e.g. "Sign in with...")
      name: 'Credentials',
      credentials: {
        username: { label: 'Username', type: 'text' },
        password: { label: 'Password', type: 'password' },
      },
      async authorize(credentials) {
        if (!credentials) {
          throw new Error('No credentials provided');
        }

        const result = await api.auth.signIn({ credentials });

        if (!result) {
          throw new Error('Invalid credentials');
        }

        return result;
      },
    }),
  ],
  events: {
    async signIn({ user }) {
      Logger.info('User signed in as a', user.userName);
    },
    // async signOut() {
    //   Logger.info('User signed out');
    // },
    // async createUser({ user }) {
    //   Logger.info('User created', user);
    // },
    // async updateUser({ user }) {
    //   Logger.info('User updated', user);
    // },
    // async linkAccount({ user, account }) {
    //   Logger.info('Account linked', { user, account });
    // },
    // async session({ session }) {
    //   Logger.info('Session', session);
    // },
  },
  callbacks: {
    async jwt({ token, user }) {
      const newToken = { ...token };
      if (user) {
        newToken.token = user.token;
        newToken.expiresIn = user.expiresIn;
        newToken.expireDate = user.expireDate;
        newToken.userName = user.userName;
        newToken.claims = user.claims;
      }

      /**
       * We consider the token as expired 5 minutes before it actually expires.
       * This is because we need a valid token to generate a new token before it expires through the refresh token API.
       */
      if (newToken.expireDate && isTokenExpired(newToken.expireDate, 10)) {
        const result = await api.auth.refreshToken({ token: newToken.token });
        return result;
      }

      return newToken;
    },
    async session({ token, session }) {
      const newSession = { ...session };
      if (token?.token) {
        newSession.user = {
          token: token.token,
          expiresIn: token.expiresIn,
          expireDate: token.expireDate,
          userName: token.userName,
        };
      }
      return newSession;
    },
  },
} satisfies AuthOptions;

export async function getAuthSession(
  ...args:
    | [GetServerSidePropsContext['req'], GetServerSidePropsContext['res']]
    | [NextApiRequest, NextApiResponse]
    | []
) {
  try {
    const session = await getServerSession(...args, authConfig);
    return session;
  } catch (error: unknown) {
    Logger.error('Failed to get session', error);
    return null;
  }
}

export async function getAuthUser(
  ...args:
    | [GetServerSidePropsContext['req'], GetServerSidePropsContext['res']]
    | [NextApiRequest, NextApiResponse]
    | []
) {
  const session = await getAuthSession(...args);
  return session?.user;
}
