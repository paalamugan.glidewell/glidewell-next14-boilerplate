import 'next-auth';

/**
 * Module augmentation for `next-auth` types.
 * Allows us to add custom properties to the `session` object and keep type
 * safety.
 *
 * @see https://next-auth.js.org/getting-started/typescript#module-augmentation
 * */
declare module 'next-auth' {
  interface User {
    id?: string;
    userName: string;
    token: string;
    expiresIn: number;
    expireDate: string;
    claims: string[];
  }
  /**
   * Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
   */
  interface Session {
    user: Omit<User, 'claims'>;
    accessToken?: string;
  }
}

declare module 'next-auth/jwt' {
  /** Returned by the `jwt` callback and `getToken`, when using JWT sessions */
  interface JWT {
    /** User ID */
    id?: string;
    userName: string;
    claims: string[];
    expiresIn: number;
    token: string;
    expireDate: string;
  }
}
