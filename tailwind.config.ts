/* eslint-disable import/no-extraneous-dependencies */
import tailwindConfig from '@glidewell/react-config/tailwind';
import type { Config } from 'tailwindcss';
import { fontFamily } from 'tailwindcss/defaultTheme';

const config = {
  presets: [tailwindConfig],
  content: [
    './src/**/*.{js,ts,jsx,tsx}',
    './node_modules/@glidewell/react-components/dist/**/*.{js,ts,jsx,tsx}',
    './node_modules/@glidewell/react-layouts/dist/**/*.{js,ts,jsx,tsx}',
    './node_modules/@glidewell/react-shared/dist/**/*.{js,ts,jsx,tsx}',
    './node_modules/@glidewell/react-icons/dist/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ['var(--font-sans)', ...fontFamily.sans],
        'open-sans': ['var(--font-open-sans)'],
      },
    },
  },
} satisfies Config;

export default config;
