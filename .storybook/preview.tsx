import '../src/styles/globals.css';

import { NextThemeProvider } from '@glidewell/react-providers/NextThemeProvider';
import type { Preview } from '@storybook/react';

const preview: Preview = {
  parameters: {
    nextjs: { router: { basePath: '' }, appDirectory: true },
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
  decorators: [
    Story => (
      <NextThemeProvider>
        <Story />
      </NextThemeProvider>
    ),
  ],
};

export default preview;
