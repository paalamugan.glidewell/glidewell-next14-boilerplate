# Glidewell Next.js 14+ Boilerplate with Next Auth, Tailwind CSS 3.4, and TypeScript

Glidewell Next.js 14+ Boilerplate with App Router support, Next Auth, TRPC, Tailwind CSS, and TypeScript ⚡️ Prioritizing developer experience first: Next.js, TypeScript, ESLint, Prettier, Husky, Lint-Staged, Jest, Testing Library, Commitlint, VSCode, PostCSS, Tailwind CSS, Authentication with [NextAuth](https://next-auth.js.org/), Storybook and more.

Clone this project and use it to create your own [Next.js](https://nextjs.org) project. This project is a minimalistic boilerplate for Next.js with the following features:

## Features

Developer experience first, extremely flexible code structure and only keep what you need:

- ⚡ [Next.js](https://nextjs.org) with App Router support
- 🔥 Type checking [TypeScript](https://www.typescriptlang.org)
- 💎 Integrate with [Tailwind CSS](https://tailwindcss.com)
- ✅ Strict Mode for TypeScript and React 18
- 🔒 Authentication with [Next Auth](https://next-auth.js.org/): Sign up, Sign in, Sign out.
- ♻️ Type-safe environment variables with T3 Env
- ⌨️ Form handling with React Hook Form
- 🔴 Validation library with Zod
- 📏 Linter with [ESLint](https://eslint.org) (default Next.js, Next.js Core Web Vitals, Tailwind CSS and Airbnb configuration)
- 💖 Code Formatter with [Prettier](https://prettier.io)
- 🦊 Husky for Git Hooks
- 🚫 Lint-staged for running linters on Git staged files
- 🚓 Lint git commit with Commitlint
- 📓 Write standard compliant commit messages with Commitizen
- 🦺 Unit Testing with Jest and React Testing Library
- 🧪 Integration and E2E Testing with Playwright
- 👷 Run tests on pull request with GitHub Actions
- 🎉 Storybook for UI development
- 🎁 Automatic changelog generation with Semantic Release
- 🔍 Visual testing with Percy (Optional)
- 💡 Absolute Imports using `@` prefix
- 🗂 VSCode configuration: Debug, Settings, Tasks and Extensions
- ⚙️ [Bundler Analyzer](https://www.npmjs.com/package/@next/bundle-analyzer)

Built-in feature from Next.js:

- ☕ Minify HTML & CSS
- 💨 Live reload
- ✅ Cache busting

### Philosophy

- Nothing is hidden from you, so you have the freedom to make the necessary adjustments to fit your needs and preferences.
- Easy to customize
- Minimal code
- 🚀 Production-ready

### Requirements

- Node.js 20+ and npm. You can install it by running:

```sh
nvm install 20.12.2 && nvm use 20.12.2
```

- [pnpm](https://pnpm.io) package manager. You can install it by running:

```sh
npm install -g pnpm
```

or follow the instructions on the [pnpm installation](https://pnpm.io/installation).

### Getting started

Run the following command on your local environment:

```shell
git clone https://gitlab.com/paalamugan.glidewell/glidewell-next14-boilerplate.git my-project-name
cd my-project-name
pnpm install
```

#### Prerequisites

- Copy the `.env.example` file to `.env.local` and update the environment variables with your own values.

```shell
cp .env.example .env.local
```

and **update the environment variables with your own values**.

Then, you can run the project locally in development mode with live reload by executing:

```shell
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your favorite browser to see your project.

### Project structure

```shell
.
├── README.md                       # README file
├── .husky                          # Husky configuration
├── .storybook                      # Storybook folder
├── .vscode                         # VSCode configuration
├── public                          # Public assets folder
├── scripts                         # Scripts folder
├── src
│   ├── app                         # Next JS App (App Router)
│   ├── components                  # React components
│   ├── constants                   # constants folder
│   ├── server                      # server folder
│   ├── stores                      # Store folder (Zustand)
│   ├── hooks                       # hooks folder
│   |   ├── react-client            # client side hooks
│   |   ├── react-server            # server side hooks
│   |   ├── react-generic           # generic hooks (both client and server)
│   ├── layouts                     # layouts folder
│   ├── lib                         # 3rd party libraries configuration
│   ├── providers                   # providers folder
│   ├── styles                      # Styles folder
│   ├── templates                   # Templates folder
│   ├── trpc                        # tRPC folder
│   ├── types                       # Type definitions
│   ├── utils                       # Utilities folder
│   └── validations                 # Validation schemas(Zod)
├── tests
│   ├── e2e                         # E2E tests, also includes Monitoring as Code
│   └── integration                 # Integration tests
├── tailwind.config.js              # Tailwind CSS configuration
└── tsconfig.json                   # TypeScript configuration
```

### Customization

You can easily configure Next js Boilerplate by making a search in the whole project with `FIXME:` for making quick customization. Here is some of the most important files to customize:

- `package.json`: change the name, version, description, author, repository, etc.
- `src/constants/app-config.constant.ts`: configuration file for the app
- `src/constants/site-navigation.constant.ts`: navigation configuration file
- `.env.local`: default local environment variables

You have access to the whole code source if you need further customization. The provided code is only example for you to start your project. The sky is the limit 🚀.

### Commit Message Format

The project enforces [Conventional Commits](https://www.conventionalcommits.org/) specification. This means that all your commit messages must be formatted according to the specification. To help you write commit messages, the project uses [Commitizen](https://github.com/commitizen/cz-cli), an interactive CLI that guides you through the commit process. To use it, run the following command:

```shell
pnpm commit
```

One of the benefits of using Conventional Commits is that it allows us to automatically generate a `CHANGELOG` file. It also allows us to automatically determine the next version number based on the types of commits that are included in a release.

### Testing

All unit tests are located with the source code inside the same directory. So, it makes it easier to find them. The project uses Jest and React Testing Library for unit testing. You can run the tests with:

```shell
pnpm test
```

### Integration & E2E Testing

The project uses Playwright for Integration and E2E testing. You can run the tests with:

```shell
npx playwright install # Only for the first time in a new environment
pnpm test:e2e
```

### Deploy to production

Then, you can generate a production build with:

```shell
pnpm build
```

It generates an optimized production build of the boilerplate. For testing the generated build, you can run:

```shell
pnpm start
```

The command starts a local server with the production build. Then, you can now open [http://localhost:3000](http://localhost:3000) with your favorite browser to see the project.

### Useful commands

#### Bundle Analyzer

Next.js Boilerplate comes with a built-in bundle analyzer. It can be used to analyze the size of your JavaScript bundles. To begin, run the following command:

```shell
pnpm build-stats
```

By running the command, it'll automatically open a new browser window with the results.

### VSCode information (optional)

If you are VSCode users, you can have a better integration with VSCode by installing the suggested extension in `.vscode/extension.json`. The starter code comes up with Settings for a seamless integration with VSCode. The Debug configuration is also provided for frontend and backend debugging experience.

With the plugins installed on your VSCode, ESLint and Prettier can automatically fix the code and show you the errors. Same goes for testing, you can install VSCode Jest extension to automatically run your tests and it also show the code coverage in context.

Pro tips: if you need a project wide type checking with TypeScript, you can run a build with `Cmd` + `Shift` + `B` on Mac.
